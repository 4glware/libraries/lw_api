use rocket::{Build, Rocket};
use lw_api_common::{get_configuration, setup_configuration};
use crate::catchers::setup_default_catchers;
use crate::initial_logger::log_information;
use crate::modules::setup_modules;

pub fn setup_server(mut rocket: Rocket<Build>) -> Rocket<Build> {
    rocket = setup_configuration(rocket);
    rocket = setup_modules(rocket);
    let configuration = get_configuration();
    if configuration.use_default_catchers == true {
        rocket = setup_default_catchers(rocket);
    }
    rocket = log_information(rocket);
    return rocket;
}