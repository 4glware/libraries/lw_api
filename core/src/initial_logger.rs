use figment::Figment;
use rocket::{Build, Rocket};
use rocket::log::private::Level;
use lw_api_common::{colored_debug, colored_info, get_configuration};
use lw_api_common::log_utils::{bool_to_string, log_catchers_table, log_routes_table, log_table};
use lw_api_common::logging::LogColor;

pub fn log_information(mut rocket: Rocket<Build>) -> Rocket<Build> {
    colored_info("Starting LWApi...", LogColor::GREEN);
    let configuration = get_configuration();
    if configuration.modules.is_some() {
        let modules = configuration.modules.unwrap();
        if modules.security.is_some() && modules.security.unwrap().enabled {
            colored_info("Using security module", LogColor::GREEN);
        }
        if modules.storage.is_some() && modules.storage.unwrap().enabled {
            colored_info("Using storage module", LogColor::GREEN);
        }
        if modules.email.is_some() && modules.email.unwrap().enabled {
            colored_info("Using email module", LogColor::GREEN);
        }
        if modules.billing.is_some() && modules.billing.unwrap().enabled {
            colored_info("Using billing module", LogColor::GREEN);
        }
    }
    colored_info("Starting Rocket server...", LogColor::GREEN);
    colored_debug("Rocket Configuration:", LogColor::BLUE);
    let figment: Figment = rocket.figment().clone();
    #[cfg(debug_assertions)]
    colored_debug("Configured for debug", LogColor::BLUE);
    #[cfg(not(debug_assertions))]
    colored_debug("Configured for debug", LogColor::RED);
    let base_path = format!("http://{}:{}", configuration.address, configuration.port);
    colored_debug(format!("Running at: {}", base_path.clone()).as_str(), LogColor::BLUE);
    colored_debug(format!("Workers Available = {}", configuration.workers).as_str(), LogColor::BLUE);
    colored_debug(format!("Ident = {}", configuration.ident).as_str(), LogColor::BLUE);
    colored_debug(format!("Keep Alive Time = {}", configuration.keep_alive).as_str(), LogColor::BLUE);
    colored_debug(format!("Temporal Directory = {}", configuration.temp_dir).as_str(), LogColor::BLUE);
    if configuration.log_file.is_some() {
        colored_debug(format!("Log File = {}", configuration.log_file.unwrap()).as_str(), LogColor::BLUE);
    }
    colored_debug(format!("Log Level = {}", configuration.log_level).as_str(), LogColor::BLUE);
    colored_debug(format!("Cli Colors = {}", bool_to_string(configuration.cli_colors)).as_str(), LogColor::BLUE);
    colored_debug(format!("Secret Key = {}", bool_to_string(configuration.secret_key.is_some())).as_str(), LogColor::BLUE);

    if figment.find_value("limits").is_ok() {
        log_table(Level::Debug, figment.find_value("limits").unwrap(), "Limits");
    }
    if figment.find_value("tls").is_ok() {
        log_table(Level::Debug, figment.find_value("tls").unwrap(), "TSL");
    }
    if figment.find_value("shutdown").is_ok() {
        log_table(Level::Debug, figment.find_value("shutdown").unwrap(), "Shutdown");
    }
    colored_debug(format!("Default Catchers = {}", bool_to_string(configuration.use_default_catchers)).as_str(), LogColor::BLUE);
    rocket = log_routes_table(Level::Debug, rocket, base_path);
    rocket = log_catchers_table(Level::Debug, rocket);
    return rocket;
}