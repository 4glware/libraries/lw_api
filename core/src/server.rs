use rocket::{Build, Rocket};
use lw_api_common::{await_main_thread, init_log};
use crate::setup::setup_server;

pub struct ServerBuilder {
    rocket: Option<Rocket<Build>>
}

impl ServerBuilder {
    pub fn build() -> Self {
        init_log("log4rs.yml", Default::default()).unwrap();
        ServerBuilder {
            rocket: None
        }
    }
    pub fn build_with_log(log_file: String) -> Self {
        init_log(log_file, Default::default()).unwrap();
        ServerBuilder {
            rocket: None
        }
    }
    pub fn instance(self, rocket: Rocket<Build>) -> Self {
        ServerBuilder {
            rocket: Some(setup_server(rocket))
        }
    }
    pub fn new_instance(self) -> Self {
        ServerBuilder {
            rocket: Some(setup_server(Rocket::build()))
        }
    }
    pub fn start(self) {
        await_main_thread(async {
            let rocket = self.rocket.unwrap();
            rocket.launch().await
        }).unwrap();
    }
}

