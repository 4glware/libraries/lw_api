use rocket::{Build, Rocket};
use lw_api_common::{get_configuration};
use lw_api_codegen::provide_module_features;

#[provide_module_features("security", "storage", "email", "billing")]
pub struct Modules;

pub fn setup_modules(mut rocket: Rocket<Build>) -> Rocket<Build> {
    let configuration = get_configuration();
    if configuration.modules.is_some() {
        let modules = configuration.modules.unwrap();
        if modules.security.is_some() && modules.security.unwrap().enabled {
            rocket = Modules::setup_security(rocket);
        }
        if modules.storage.is_some() && modules.storage.unwrap().enabled {
            rocket = Modules::setup_storage(rocket);
        }
        if modules.email.is_some() && modules.email.unwrap().enabled {
            rocket = Modules::setup_email(rocket);
        }
        if modules.billing.is_some() && modules.billing.unwrap().enabled {
            rocket = Modules::setup_billing(rocket);
        }
    }
    rocket
}