use rocket::http::Status;
use rocket::{Build, catchers, catch, Request, Rocket};
use lw_api_common::{ApiError, debug};

#[catch(500)]
fn internal_server_error() -> ApiError {
    ApiError {
        status: Status::InternalServerError.code,
        error: "Unknown internal server error, you should check the logs".to_string()
    }
}

#[catch(404)]
fn not_found(request: &Request) -> ApiError {
    ApiError {
        status: Status::NotFound.code,
        error: format!("Request {} Not found, wrong url.", request.uri())
    }
}

#[catch(403)]
fn forbidden() -> ApiError {
    ApiError {
        status: Status::Forbidden.code,
        error: "Invalid Token".to_string()
    }
}

#[catch(default)]
fn default_catcher(status: Status, request: &Request) -> ApiError {
    ApiError {
        status: status.code,
        error: format!("Unknown error on request {}, please check the logs.", request.uri())
    }
}

pub fn setup_default_catchers(rocket: Rocket<Build>) -> Rocket<Build> {
    debug!("Using default catchers...");
    rocket.register("/", catchers![internal_server_error, not_found, forbidden, default_catcher])
}