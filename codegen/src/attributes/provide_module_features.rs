use proc_macro::{TokenStream};
use inflector::cases::snakecase::to_snake_case;
use proc_macro2::Span;
use syn::{AttributeArgs, NestedMeta, Lit};

pub fn implementation(attr: TokenStream, input: TokenStream) -> TokenStream {
    use quote::quote;

    let input = syn::parse_macro_input!(input as syn::ItemStruct);
    let name = &input.ident;
    let vis = &input.vis;

    let args: AttributeArgs = syn::parse_macro_input!(attr as syn::AttributeArgs);

    let mut module_names: Vec<String> = Vec::new();

    for arg in args {
        match arg {
            NestedMeta::Lit(base) => {
                match base {
                    Lit::Str(name) => {
                        module_names.push(name.value());
                    },
                    _=> {}
                }
            },
            _=> {}
        }
    }

    if module_names.is_empty() {
        panic!("You have to specify at least a name for the modules you want to provide");
    }

    let mut setup_functions: Vec<proc_macro2::TokenStream> = Vec::new();

    for module_name in module_names {
        let setup_function_name = syn::Ident::new(format!("setup_{}", to_snake_case(module_name.as_str())).as_str(), Span::call_site());
        let module_name_ident = syn::Ident::new(format!("lw_api_{}", module_name).as_str(), Span::call_site());
        setup_functions.push(quote! {
            #[cfg(feature = #module_name)]
            pub fn #setup_function_name(rocket: Rocket<Build>) -> Rocket<Build> {
                #module_name_ident::setup(rocket)
            }

            #[cfg(not(feature = #module_name))]
            pub fn #setup_function_name(rocket: Rocket<Build>) -> Rocket<Build> {
                use lw_api_common::stop;
                stop!("You need to enable the feature \"{}\" in the Cargo.toml file under the \"lw_api\" dependency in order to use it.", #module_name);
            }
        });
    }

    (quote! {
        #vis struct #name;
        impl #name {
            #(#setup_functions)*
        }
    }).into()
}