use proc_macro::{TokenStream};
use inflector::cases::snakecase::to_snake_case;
use quote::ToTokens;
use syn::{Item, Stmt};
use crate::generic::macros::{ParamBuilder};

pub fn implementation(attr: TokenStream, input: TokenStream) -> TokenStream {
    use quote::quote;

    let mut input = syn::parse_macro_input!(input as syn::ItemFn);
    let vis = &input.vis;
    let sig = &mut input.sig;
    let body = &input.block;
    let name = &sig.ident;

    let default_controller_path = format!("/{}", to_snake_case(name.clone().to_string().as_str()));

    let attributes = syn::parse_macro_input!(attr as syn::AttributeArgs);

    let base_path = ParamBuilder::build(attributes.clone(), true, false)
        .string("base_path", default_controller_path.as_str()).unwrap();

    let secured = ParamBuilder::build(attributes.clone(), false, false)
        .boolean("global_secured", false).unwrap();

    let mut service_functions: Vec<proc_macro2::TokenStream> = Vec::new();

    for statement in &body.stmts {
        match statement {
            Stmt::Item(item) => {
                match item {
                    Item::Fn(function) => {
                        let visibility = &function.vis;
                        let async_token = &function.sig.asyncness;
                        let function_name = &function.sig.ident;
                        let return_type = &function.sig.output;
                        let attributes = &function.attrs;
                        let input = &function.sig.inputs;
                        let body = &function.block;
                        let authorized_context = *&function.sig.inputs.iter().filter(|input| input.to_token_stream().to_string().contains("AuthorizedUser")).count() > 0;
                        if secured && !authorized_context {
                            service_functions.push(quote! {
                               #(#attributes)*
                                #visibility #async_token fn #function_name(#input, auth_user: lw_api_database::models::security::AuthorizedUser) #return_type #body
                                rocket = rocket.mount(#base_path, routes![#function_name]);
                            });
                        } else {
                            service_functions.push(quote! {
                               #function
                                rocket = rocket.mount(#base_path, routes![#function_name]);
                            });
                        }
                    },
                    _ => {}
                }
            },
            _ => {}
        }
    }

    (quote! {

        use rocket::{Rocket, Build, routes};

        #[allow(dead_code)]
        #vis fn #name() -> &'static dyn Fn(Rocket<Build>) -> Rocket<Build> {
            fn builder(mut rocket: Rocket<Build>) -> Rocket<Build> {
                #body
                #(#service_functions)*
                rocket
            }
            return &builder;
        }
    }).into()
}