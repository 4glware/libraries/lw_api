use proc_macro::TokenStream;
use inflector::cases::snakecase::to_snake_case;
use proc_macro2::{Span, Literal};
use syn::Type;
use crate::generic::{attributes::get_literal_param};

pub fn implementation(input: TokenStream) -> TokenStream {
    use quote::quote;

    let input = syn::parse_macro_input!(input as syn::ItemStruct);
    let name = &input.ident;
    let partial_name = syn::Ident::new(format!("{}Partial", name.to_string()).as_str(), Span::call_site());
    let fields = &input.fields;

    let mut entity_fields_for_document: Vec<proc_macro2::TokenStream> = Vec::new();
    let mut partial_entity_fields: Vec<proc_macro2::TokenStream> = Vec::new();

    for field in fields {
        let field_ident = &field.ident.as_ref().unwrap().clone();
        match &field.ty {
            Type::Path(type_path) => {
                let type_ident = type_path.path.segments.first().unwrap();
                if type_ident.ident.to_string() == "Option".to_string() {
                    partial_entity_fields.push(quote! {
                        #[serde(skip_serializing_if = "Option::is_none")]
                        #field_ident: #type_ident,
                    });
                } else {
                    partial_entity_fields.push(quote! {
                        #[serde(skip_serializing_if = "Option::is_none")]
                        #field_ident: Option<#type_ident>,
                    });
                }
            },
            _=> {}
        }
        entity_fields_for_document.push(quote! {
            stringify!(#field_ident): self.#field_ident.clone(),
        });
    }

    let mut entity_field_names: Vec<proc_macro2::TokenStream> = Vec::new();

    for field in fields {
        let field_ident = field.ident.as_ref().unwrap().clone();
        entity_field_names.push(quote! {
            stringify!(#field_ident)
        });
    }

    let collection_name = get_literal_param(
        &input.attrs,
        "collection_name",
        None,
        false
    ).unwrap_or(
        Literal::string(to_snake_case(name.to_string().as_str()).as_str())
    ).to_string().replace("\"", "");

    return (quote! {
        #[derive(Debug, Clone, Serialize, Deserialize)]
        pub struct #partial_name {
            #(#partial_entity_fields)*
        }
        impl Entity for #name {
            fn get_id(self) -> Option<String> {
                return self._id;
            }
            fn get_collection() -> String {
                #collection_name.to_string()
            }
            fn get_name() -> String {
                stringify!(#name).to_string()
            }
            fn get_document(&self) -> mongodb::bson::Document {
                mongodb::bson::doc! {
                    #(#entity_fields_for_document)*
                }
            }
            fn get_query_by_id_document(&self) -> mongodb::bson::Document {
                mongodb::bson::doc! {
                    "_id": self._id.clone()
                }
            }
            fn get_search_filter_document(search_filter: lw_api_common::SearchFilter) -> Result<mongodb::bson::Document, lw_api_common::ApiError> {
                let query_joiner = match search_filter.clone().query_joiner {
                    Some(joiner) => match joiner {
                        lw_api_common::SearchQueryJoiner::And => "$and",
                        lw_api_common::SearchQueryJoiner::Or => "$or"
                    },
                    None => "$and"
                };
                if search_filter.clone().fields.is_empty() {
                    return match search_filter.clone().filter_type {
                        Some(filter) => {
                          return match filter {
                            lw_api_common::SearchFilterType::Equals => Ok(mongodb::bson::doc! {
                               query_joiner: [ #({#entity_field_names: search_filter.text.clone()},)* ]
                            }),
                            lw_api_common::SearchFilterType::Contains => Ok(mongodb::bson::doc! {
                                query_joiner: [
                                    #({#entity_field_names: {
                                        "$regex": format!("{}", search_filter.text.clone()),
                                        "$options": "i",
                                    }},)*
                                ]
                            }),
                            lw_api_common::SearchFilterType::StartsWith => Ok(mongodb::bson::doc! {
                                query_joiner: [
                                    #({#entity_field_names: {
                                        "$regex": format!("^{}", search_filter.text.clone()),
                                        "$options": "i",
                                    }},)*
                                ]
                            }),
                          }
                        },
                        None => Ok(mongodb::bson::doc! {
                           query_joiner: [ #({#entity_field_names: search_filter.text.clone()},)* ]
                        })
                    }
                } else {
                    let mut valid_field_names = std::collections::HashSet::new();
                    for field_name in vec![#(#entity_field_names,)*] {
                        valid_field_names.insert(field_name.to_string());
                    }
                    for field in search_filter.clone().fields {
                        if valid_field_names.get(field.as_str()).is_none() {
                            return Err(lw_api_common::ApiError::from_message(rocket::http::Status::BadRequest, format!("Invalid field name {}", field)));
                        }
                    }
                    return match search_filter.clone().filter_type {
                        Some(filter) => {
                          return match filter {
                            lw_api_common::SearchFilterType::Equals => Ok(mongodb::bson::doc! {
                                query_joiner: search_filter.clone().fields.iter().map(|field| mongodb::bson::doc! {field: search_filter.text.clone()}).collect::<Vec<mongodb::bson::Document>>()
                            }),
                            lw_api_common::SearchFilterType::Contains => Ok(mongodb::bson::doc! {
                                query_joiner: search_filter.clone().fields.iter().map(|field| mongodb::bson::doc! {field: {
                                    "$regex": format!("{}", search_filter.text.clone()),
                                    "$options": "i",
                                }}).collect::<Vec<mongodb::bson::Document>>()
                            }),
                            lw_api_common::SearchFilterType::StartsWith => Ok(mongodb::bson::doc! {
                                query_joiner: search_filter.clone().fields.iter().map(|field| mongodb::bson::doc! {field: {
                                    "$regex": format!("{}", search_filter.text.clone()),
                                    "$options": "i",
                                }}).collect::<Vec<mongodb::bson::Document>>()
                            }),
                          }
                        },
                        None => Ok(mongodb::bson::doc! {
                           query_joiner: search_filter.clone().fields.iter().map(|field| mongodb::bson::doc! {field: search_filter.text.clone()}).collect::<Vec<mongodb::bson::Document>>()
                        })
                    };
                }
            }
            fn get_projection_document(filter_fields: Option<Vec<String>>) -> Result<mongodb::bson::Document, lw_api_common::ApiError> {
                let filter_fields = match filter_fields {
                    Some(filters) => filters,
                    None => Vec::new()
                };
                let mut valid_field_names = std::collections::HashSet::new();
                for field_name in vec![#(#entity_field_names,)*] {
                    valid_field_names.insert(field_name.to_string());
                }
                let mut document: mongodb::bson::Document = mongodb::bson::Document::new();
                for field in filter_fields {
                    if valid_field_names.get(field.as_str()).is_none() {
                        return Err(lw_api_common::ApiError::from_message(rocket::http::Status::BadRequest, format!("Invalid field name {}", field)));
                    }
                    document.insert(field.as_str(), 1);
                }
                return Ok(document);
            }
        }
    }).into()
}