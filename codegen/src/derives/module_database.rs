use proc_macro::{TokenStream};
use proc_macro2::{Literal};
use inflector::cases::snakecase::to_snake_case;
use crate::generic::{attributes::get_literal_param};

pub fn implementation(input: TokenStream) -> TokenStream {
    use quote::quote;

    let input = syn::parse_macro_input!(input as syn::ItemStruct);
    let name = &input.ident;

    let database_name= get_literal_param(
        &input.attrs,
        "database_name",
        None,
        false)
        .unwrap_or(Literal::string(to_snake_case(name.to_string().as_str()).as_str()))
        .to_string().replace("\"", "");

    (quote! {
        impl lw_api_common::ModuleDatabase for #name {
            fn new() -> Self {
                #name {}
            }
            fn get_module_name() -> String {
                #database_name.to_string()
            }
        }
    }).into()
}