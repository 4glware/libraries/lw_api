use proc_macro::TokenStream;

pub fn implementation(input: TokenStream) -> TokenStream {
    use quote::quote;

    let input = syn::parse_macro_input!(input as syn::ItemStruct);
    let ident = &input.ident;

    return (quote! {
        #[rocket::async_trait]
        impl<'r> rocket::data::FromData<'r> for #ident {
            type Error = lw_api_common::errors::ParsingError;

            async fn from_data(req: &'r rocket::Request<'_>, data: rocket::data::Data<'r>) -> rocket::data::Outcome<'r, #ident> {
                async fn parse_from_data<'r>(req: &'r rocket::Request<'_>, data: rocket::data::Data<'r>) -> Result<#ident, lw_api_common::errors::ParsingError> {
                    let limit = req.limits().get("json").unwrap_or(rocket::data::Limits::JSON);
                    let string = match data.open(limit).into_string().await {
                        Ok(s) if s.is_complete() => s.into_inner(),
                        Ok(_) => {
                            let eof = std::io::ErrorKind::UnexpectedEof;
                            return Err(lw_api_common::errors::ParsingError::Io(std::io::Error::new(eof, "data limit exceeded")));
                        },
                        Err(e) => return Err(lw_api_common::errors::ParsingError::Io(e)),
                    };
                    serde_json::from_str::<#ident>(string.as_str()).map_err(|e| lw_api_common::errors::ParsingError::Parse(string, e))
                }
                match parse_from_data(req, data).await {
                    Ok(value) => rocket::data::Outcome::Success(value),
                    Err(lw_api_common::errors::ParsingError::Io(e)) if e.kind() == std::io::ErrorKind::UnexpectedEof => {
                        rocket::data::Outcome::Failure((rocket::http::Status::PayloadTooLarge, lw_api_common::errors::ParsingError::Io(e)))
                    },
                    Err(lw_api_common::errors::ParsingError::Parse(s, e)) if e.classify() == serde_json::error::Category::Data => {
                        rocket::data::Outcome::Failure((rocket::http::Status::UnprocessableEntity, lw_api_common::errors::ParsingError::Parse(s, e)))
                    },
                    Err(e) => rocket::data::Outcome::Failure((rocket::http::Status::BadRequest, e)),

                }
            }
        }
    }).into()
}