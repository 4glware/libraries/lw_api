use proc_macro::TokenStream;

pub fn implementation(input: TokenStream) -> TokenStream {
    use quote::quote;

    let input = syn::parse_macro_input!(input as syn::ItemStruct);
    let ident = &input.ident;
    let generics = &input.generics.params;

    let definition_header: proc_macro2::TokenStream;

    if generics.len() > 0 {
        definition_header = quote! {
            impl <'r, #generics: serde::Serialize + std::fmt::Debug> rocket::response::Responder<'r, 'static> for #ident<#generics>
        };
    } else {
        definition_header = quote! {
            impl <'r> rocket::response::Responder<'r, 'static> for #ident
        };
    }

    return (quote! {
        #definition_header {
            fn respond_to(self, _: &'r rocket::Request<'_>) -> rocket::response::Result<'static> {
                let json_body = serde_json::to_string(&self);
                return match json_body {
                    Ok(json ) => {
                        rocket::Response::build()
                            .sized_body(json.len(), std::io::Cursor::new(json))
                            .header(rocket::http::ContentType::JSON)
                            .status(rocket::http::Status::Ok)
                            .ok()
                    },
                    Err(error) => {
                        let api_error = lw_api_common::errors::ApiError {
                            status: 500,
                            error: error.to_string()
                        };
                        let api_error_json = serde_json::to_string(&api_error).unwrap();
                        rocket::Response::build()
                            .sized_body(api_error_json.len(), std::io::Cursor::new(api_error_json))
                            .header(rocket::http::ContentType::JSON)
                            .status(rocket::http::Status::InternalServerError)
                            .ok()
                    }
                }

            }
        }
    }).into()
}