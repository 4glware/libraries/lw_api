use proc_macro::{TokenStream};
use proc_macro2::Ident;
use crate::generic::{attributes::get_ident_param};

pub fn implementation(input: TokenStream) -> TokenStream {
    use quote::quote;

    let input = syn::parse_macro_input!(input as syn::ItemStruct);
    let ident = &input.ident;
    let fields = &input.fields;

    let mut repositories: Vec<proc_macro2::TokenStream> = Vec::new();

    for field in fields {
        let field_ident = field.clone().ident.unwrap();
        repositories.push(quote! {
           #field_ident: lw_api_database::Repository::new(database.clone().connect().await.db),
        });
    }

    let database: Ident = get_ident_param(&input.attrs, "service",Some("db_type"), true).unwrap();
    let initialized_service: Option<Ident> = get_ident_param(&input.attrs, "service",Some("initialized_service"), false);
    let initialized_service = initialized_service.is_some() && initialized_service.unwrap().to_string().eq("true");

    let mut initialization: proc_macro2::TokenStream = quote ! {
        impl #ident {
            async fn new(database: #database) -> Self {
                #ident {
                    #(#repositories)*
                }
            }
        }
    };

    if initialized_service {
        initialization = quote ! {
            impl #ident {
                async fn new(database: #database) -> Self {
                    let mut instance = #ident {
                        #(#repositories)*
                    };
                    instance.initialize();
                    return instance;
                }
            }
        }
    }

    return (quote! {

        #initialization

        #[rocket::async_trait]
        impl <'r> rocket::request::FromRequest<'r> for #ident {
            type Error = ();

            async fn from_request(request: &'r rocket::request::Request<'_>) -> rocket::request::Outcome<Self, Self::Error> {
                rocket::request::Outcome::Success(#ident::new(#database::from_request(request).await.unwrap()).await)
            }
        }
    }).into()
}