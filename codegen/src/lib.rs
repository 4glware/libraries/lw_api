extern crate core;

use proc_macro::{TokenStream};

mod derives;
mod attributes;
mod generic;


/** Proc macro to describe a controller function.
    Example:

    ```rust
     #[lw_api_codegen::controller("/some_controller")]
            pub fn some_controller() {
                #[post("/some_method", format = "application/json", data = "<request>")]
                async fn some_method(request: SomeRequest) {
                    // ....
                }
            }
    ```

    ```rust
      #[lw_api_codegen::controller(base_path = "/some_controller", global_secured = true)]
      pub fn some_controller() {
          #[post("/some_method", format = "application/json", data = "<request>")]
           async fn some_method(request: SomeRequest) {
              // ....
           }
      }
    ```
    @param base_path the base path url for the controller.
    @param global_secured injects the AuthorizedUser guard type on all the methods for this controller that doesn't have the context already.

 **/
#[proc_macro_attribute]
pub fn controller(attr: TokenStream, input: TokenStream) -> TokenStream {
    attributes::controller::implementation(attr, input)
}

#[proc_macro_attribute]
pub fn provide_module_features(attr: TokenStream, input: TokenStream) -> TokenStream {
    attributes::provide_module_features::implementation(attr, input)
}

#[proc_macro_derive(ModuleDatabase, attributes(database_name))]
pub fn module_database(input: TokenStream) -> TokenStream {
    derives::module_database::implementation(input)
}

#[proc_macro_derive(RequestBody)]
pub fn derive_request_body(input: TokenStream) -> TokenStream {
    derives::request_body::implementation(input)
}

#[proc_macro_derive(ResponseBody)]
pub fn derive_response_body(input: TokenStream) -> TokenStream {
    derives::response_body::implementation(input)
}

#[proc_macro_derive(Service, attributes(service))]
pub fn derive_service(input: TokenStream) -> TokenStream {
    derives::service::implementation(input)
}

#[proc_macro_derive(Entity)]
pub fn derive_entity(input: TokenStream) -> TokenStream {
    derives::entity::implementation(input)
}
