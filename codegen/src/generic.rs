pub mod attributes {
    use proc_macro2::{TokenTree, Ident, Literal};
    use syn::{Attribute};

    pub fn get_literal_param(
        attributes: &Vec<Attribute>,
        attribute_name: &'static str,
        param_name: Option<&'static str>,
        required: bool) -> Option<Literal> {
        let mut result: Option<Literal> = None;
        let mut found_param = false;
        for attribute in attributes {
            for segment in &attribute.path.segments {
                if segment.ident.to_string().as_str() == attribute_name {
                    for token in attribute.tokens.clone() {
                        match token {
                            TokenTree::Group(group) => {
                                for token_type in group.stream() {
                                    match token_type {
                                        TokenTree::Literal(literal) => {
                                            if param_name.is_some() {
                                                if found_param {
                                                    result = Some(literal.clone());
                                                }
                                                if literal.to_string().as_str() == param_name.unwrap() {
                                                    found_param = true;
                                                }
                                            } else {
                                                result = Some(literal.clone());
                                            }
                                        },
                                        _=> {}
                                    }
                                    if result.is_some() {
                                        break;
                                    }
                                }
                            },
                            _=> {}
                        }
                        if result.is_some() {
                            break;
                        }
                    }
                }
                if result.is_some() {
                    break;
                }
            }
            if result.is_some() {
                break;
            }
        }
        if result.is_none() && param_name.is_some() {
            if required {
                panic!("Required parameter {} not found on attribute {}", param_name.unwrap(), attribute_name);
            }
        }
        if result.is_none() {
            if required {
                panic!("Required attribute {} not found", attribute_name);
            }
        }
        return result;
    }

    pub fn get_ident_param(
        attributes: &Vec<Attribute>,
        attribute_name: &'static str,
        param_name: Option<&'static str>,
        required: bool) -> Option<Ident> {
        let mut result: Option<Ident> = None;
        let mut found_param = false;
        for attribute in attributes {
            for segment in &attribute.path.segments {
                if segment.ident.to_string().as_str() == attribute_name {
                    for token in attribute.tokens.clone() {
                        match token {
                            TokenTree::Group(group) => {
                                for token_type in group.stream() {
                                    match token_type {
                                        TokenTree::Ident(ident) => {
                                            if param_name.is_some() {
                                                if found_param {
                                                    result = Some(ident.clone());
                                                }
                                                if ident.to_string().as_str() == param_name.unwrap() {
                                                    found_param = true;
                                                }
                                            } else {
                                                result = Some(ident.clone());
                                            }
                                        },
                                        _=> {}
                                    }
                                    if result.is_some() {
                                        break;
                                    }
                                }
                            },
                            _=> {}
                        }
                        if result.is_some() {
                            break;
                        }
                    }
                }
                if result.is_some() {
                    break;
                }
            }
            if result.is_some() {
                break;
            }
        }
        if result.is_none() && param_name.is_some() && required {
            panic!("Required parameter {} not found on attribute {}", param_name.unwrap(), attribute_name);
        }
        if result.is_none() && required {
            panic!("Required attribute {} not found", attribute_name);
        }
        return result;
    }
}

pub mod macros {
    use syn::{AttributeArgs, Lit, Meta, NestedMeta};

    pub struct ParamBuilder {
        attributes: AttributeArgs,
        take_path_as_param: bool,
        required: bool
    }

    impl ParamBuilder {
        pub fn build(attributes: AttributeArgs, take_path_as_param: bool, required: bool) -> ParamBuilder {
            return ParamBuilder {
                attributes,
                take_path_as_param,
                required
            }
        }
        pub fn string<'a>(&self, param_name: &str, default_value: impl Into<Option<&'a str>>) -> Option<String> {
            let literal = get_literal_param(self.attributes.clone(), Some(param_name), self.take_path_as_param.clone(), self.required.clone());
            if literal.is_some() {
                let literal = literal.unwrap();
                return match literal {
                    Lit::Str(value) => {
                        Some(value.value())
                    },
                    _=> {
                        None
                    }
                }
            }
            let default_value = default_value.into();
            if default_value.is_some() {
                let default_value = default_value.unwrap();
                return Some(default_value.to_string());
            }
            return None;
        }
        pub fn boolean(&self, param_name: &str, default_value: impl Into<Option<bool>>) -> Option<bool> {
            let literal = get_literal_param(self.attributes.clone(), Some(param_name), self.take_path_as_param.clone(), self.required.clone());
            if literal.is_some() {
                let literal = literal.unwrap();
                return match literal {
                    Lit::Bool(value) => {
                        Some(value.value())
                    },
                    _=> {
                        None
                    }
                }
            }
            let default_value = default_value.into();
            if default_value.is_some() {
                let default_value = default_value.unwrap();
                return Some(default_value);
            }
            return None;
        }
    }

    fn get_literal_param(
        attributes: AttributeArgs,
        param_name: Option<&str>,
        take_path_as_param: bool,
        required: bool) -> Option<Lit> {
        let mut result: Option<Lit> = None;
        for attribute in attributes {
            match attribute {
                NestedMeta::Meta(meta) => {
                    match meta {
                        Meta::NameValue(name_value) => {
                            if param_name.is_some() && name_value.path.segments.first().unwrap().ident.to_string().eq(param_name.unwrap()) {
                                result = Some(name_value.lit);
                            }
                        },
                        Meta::Path(path) => {
                            panic!("Path not supported {:?}", path);
                        },
                        Meta::List(list) => {
                            panic!("MetaList not supported {:?}", list);
                        }
                    }
                },
                NestedMeta::Lit(literal) => {
                    if take_path_as_param {
                        result = Some(literal);
                    }
                },
            }
            if result.is_some() {
                break;
            }
        }
        if result.is_none() && param_name.is_some() && required {
            panic!("Required parameter {} not found", param_name.unwrap());
        }
        if result.is_some() {
            return result;
        }
        return None;
    }
}
