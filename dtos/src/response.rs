use serde::{Serialize, Deserialize};
use lw_api_codegen::ResponseBody;

#[derive(Debug, Clone, Serialize, Deserialize, ResponseBody)]
pub struct ApiResponse<T> where T: Serialize {
    pub data: T
}
