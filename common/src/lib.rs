#![feature(map_first_last)]

extern crate core;

pub mod configuration;
pub mod runtime;
pub mod modules_configuration;
pub mod errors;
pub mod traits;
pub mod macro_rules;
pub mod logging;
pub mod log_utils;
pub mod helpers;
pub mod database;
pub mod search;

pub use helpers::*;
pub use modules_configuration::*;
pub use runtime::*;
pub use configuration::*;
pub use errors::*;
pub use traits::*;
pub use macro_rules::*;
pub use log::{debug, log, info, error, warn, trace};
pub use logging::{colored_debug, colored_warn, colored_info, colored_error, colored_trace};
pub use log4rs::init_file as init_log;
pub use database::*;
pub use search::*;
