use rocket::tokio::runtime::Builder;

pub fn await_main_thread<R>(future: impl std::future::Future<Output = R>) -> R {
    Builder::new_multi_thread()
        .thread_name("lwapi-working-thread")
        .worker_threads(1)
        .enable_all()
        .build()
        .expect("Create Tokio Runtime")
        .block_on(future)
}