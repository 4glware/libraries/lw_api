use serde::{Serialize, Deserialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SearchPagination {
    pub current_page: Option<i64>,
    pub page_size: Option<i64>
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum SearchQueryJoiner {
    And,
    Or
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum SearchFilterType {
    Equals,
    Contains,
    StartsWith,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SearchFilter {
    pub text: String,
    pub fields: Vec<String>,
    pub filter_type: Option<SearchFilterType>,
    pub query_joiner: Option<SearchQueryJoiner>
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum SortOrder {
    Asc,
    Desc
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Sort {
    sort_by: String,
    order: Option<SortOrder>
}

pub struct CalculatedPagination {
    pub total_pages: i64,
    pub start_row: i64
}