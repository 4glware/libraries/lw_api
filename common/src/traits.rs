use std::error::Error;
use mongodb::bson::Document;
use rocket::http::Status;
use crate::{ApiError, SearchFilter};

pub trait ModuleDatabase {
    fn get_module_name() -> String;
    fn new() -> Self;
}

pub trait InitializedService {
    fn initialize(&self);
}

pub trait Entity {
    fn get_id(self) -> Option<String>;
    fn get_collection() -> String;
    fn get_name() -> String;
    fn get_document(&self) -> Document;
    fn get_query_by_id_document(&self) -> Document;
    fn get_search_filter_document(search_filter: SearchFilter) -> Result<Document, ApiError>;
    fn get_projection_document(filter_fields: Option<Vec<String>>) -> Result<Document, ApiError>;
}

pub trait ParseableError {
    fn from(status: Status, error: impl Error) -> Self;
    fn from_message(status: Status, message: &str) -> Self;
}