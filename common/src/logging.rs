use log::{log};
use log::Level;

pub enum LogColor {
    BLUE,
    RED,
    GREEN,
    YELLOW
}

impl LogColor {
    pub fn code(self) -> i64 {
        match self {
            LogColor::RED => {
                31
            },
            LogColor::BLUE => {
                34
            },
            LogColor::GREEN => {
                32
            },
            LogColor::YELLOW => {
                33
            },
        }
    }
}

pub fn colored_log(level: Level, message: String, log_color: LogColor) {
    log!(level, "{}", format!("\x1b[{}m{}\x1b[0m", log_color.code(), message));
}

pub fn colored_info(message: &str, log_color: LogColor) {
    colored_log(Level::Info, message.to_string(), log_color);
}

pub fn colored_warn(message: &str, log_color: LogColor) {
    colored_log(Level::Warn, message.to_string(), log_color);
}

pub fn colored_error(message: &str, log_color: LogColor) {
    colored_log(Level::Error, message.to_string(), log_color);
}

pub fn colored_debug(message: &str, log_color: LogColor) {
    colored_log(Level::Debug, message.to_string(), log_color);
}

pub fn colored_trace(message: &str, log_color: LogColor) {
    colored_log(Level::Trace, message.to_string(), log_color);
}