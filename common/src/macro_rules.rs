#[macro_export]
macro_rules! stop {
    ($message: expr, $parameters: expr) => {
        rocket::error!($message, $parameters);
        std::process::exit(1);
    };
    ($message: expr) => {
        rocket::error!($message);
        std::process::exit(1);
    };
}