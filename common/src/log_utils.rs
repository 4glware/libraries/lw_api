use std::borrow::Cow;

use figment::value::Num::*;
use figment::value::Value;
use figment::value::Value::{Array, Bool, Char, Dict, Empty, Num, String as StringValue};
use log::Level;
use rocket::{Build, Rocket};

use crate::logging::{colored_log, LogColor};

pub fn bool_to_string(value: bool) -> String {
    match value {
        true => String::from("Enabled"),
        false => String::from("Disabled")
    }
}

pub fn value_into_string(value: Value) -> String {
    match value {
        Num(_tag, value) => {
            match value {
                U8(numeric_value) => {
                    numeric_value.to_string()
                },
                U16(numeric_value) => {
                    numeric_value.to_string()
                },
                U32(numeric_value) => {
                    numeric_value.to_string()
                },
                U64(numeric_value) => {
                    numeric_value.to_string()
                },
                U128(numeric_value) => {
                    numeric_value.to_string()
                },
                USize(numeric_value) => {
                    numeric_value.to_string()
                },
                I8(numeric_value) => {
                    numeric_value.to_string()
                },
                I16(numeric_value) => {
                    numeric_value.to_string()
                },
                I32(numeric_value) => {
                    numeric_value.to_string()
                },
                I64(numeric_value) => {
                    numeric_value.to_string()
                },
                I128(numeric_value) => {
                    numeric_value.to_string()
                },
                ISize(numeric_value) => {
                    numeric_value.to_string()
                },
                F32(numeric_value) => {
                    numeric_value.to_string()
                },
                F64(numeric_value) => {
                    numeric_value.to_string()
                }
            }
        },
        Array(_tag, value) => {
            value.iter().map(|val| value_into_string(val.clone())).collect::<Vec<String>>().join(",")
        },
        StringValue(_tag, value) => {
            value
        },
        Bool(_tag, value) => {
            bool_to_string(value)
        },
        Char(_tag, value) => {
            value.to_string()
        },
        Dict(_tag, _value) => {
            String::from("N/A")
        },
        Empty(_tag, _value) => {
            String::from("Disabled")
        }
    }
}

pub fn log_table(level: Level, value: Value, table_name: &'static str) {
    match value {
        Dict(_tag, table_tree) => {
            let mut column_names: Vec<String> = Vec::new();
            let mut values: Vec<String> = Vec::new();
            for entry in table_tree {
                let mut column_name = format!("{:^width$}", entry.0, width=entry.0.len() + 2);
                let mut value = value_into_string(entry.1);
                value = format!("{:^width$}", value, width=value.len() + 2);
                if column_name.len() > value.len() {
                    value = format!("{:^width$}", value, width=column_name.len());
                } else {
                    column_name = format!("{:^width$}", column_name, width=value.len());
                }
                column_names.push(column_name);
                values.push(value);
            }
            colored_log(level, format!("|{:-^width$}|", format!("| {} |", table_name), width=column_names.join("|").len()), LogColor::BLUE);
            colored_log(level, format!("|{}|", column_names.join("|")), LogColor::BLUE);
            colored_log(level, format!("|{:-^width$}|", "", width=column_names.join("|").len()), LogColor::BLUE);
            colored_log(level, format!("|{}|", values.join("|")), LogColor::BLUE);
            colored_log(level, format!("|{:-^width$}|", "", width=column_names.join("|").len()), LogColor::BLUE);
        },
        _=> {
            colored_log(level, format!("{} Disabled", table_name), LogColor::BLUE);
        }
    }
}

pub fn log_catchers_table(level: Level, rocket: Rocket<Build>) -> Rocket<Build> {
    let mut column_names: Vec<String> = Vec::new();
    for name in vec!("Code", "Name", "Listening Under") {
        column_names.push(format!("{:^width$}", name, width=name.len() + 2));
    }
    let mut values: Vec<Vec<String>> = Vec::new();
    for catcher in rocket.catchers().cloned() {
        let name = catcher.name.unwrap_or_else(|| Cow::Owned("N/A".to_string())).to_string();
        let code = match catcher.code.unwrap_or(0) {
            0 => { "*".to_string() },
            value=> {value.to_string()}
        };
        let base = catcher.base.to_string();
        values.push(vec![
            format!("{:^width$}", code, width= code.len() + 2),
            format!("{:^width$}", name, width= name.len() + 2),
            format!("{:^width$}", base, width= base.len() + 2),
        ]);
    }
    for row in values.clone() {
        for index in 0..column_names.len() {
            let mut column_name = column_names.get(index).unwrap().clone();
            let value = row.get(index).unwrap().clone();
            if column_name.len() < value.len() {
                column_name = format!("{:^width$}", column_name, width=value.len());
            }
            column_names[index] = column_name;
        }
    }
    let mut rows_to_show: Vec<String> = Vec::new();
    for row in values.clone() {
        let mut row_to_show: Vec<String> = Vec::new();
        for index in 0..column_names.len() {
            let column_name = column_names.get(index).unwrap().clone();
            let mut value = row.get(index).unwrap().clone();
            if column_name.len() > value.len() {
                value = format!("{:^width$}", value, width=column_name.len());
            }
            row_to_show.push(value);
        }
        rows_to_show.push(row_to_show.join("|"));
    }
    colored_log(level, format!("|{:-^width$}|", format!("| {} |", "Loaded Catchers"), width=column_names.join("|").len()), LogColor::BLUE);
    colored_log(level, format!("|{}|", column_names.join("|")), LogColor::BLUE);
    colored_log(level, format!("|{:-^width$}|", "", width=column_names.join("|").len()), LogColor::BLUE);
    for row in rows_to_show.clone() {
        colored_log(level, format!("|{}|", row), LogColor::BLUE);
    }
    colored_log(level, format!("|{:-^width$}|", "", width=column_names.join("|").len()), LogColor::BLUE);
    return rocket;
}

pub fn log_routes_table(level: Level, rocket: Rocket<Build>, base_path: String) -> Rocket<Build> {
    let mut column_names: Vec<String> = Vec::new();
    for name in vec!("Name", "Method", "Url", "Rank", "Media Type") {
        column_names.push(format!("{:^width$}", name, width=name.len() + 2));
    }
    let mut values: Vec<Vec<String>> = Vec::new();
    for route in rocket.routes().cloned() {
        let name = route.name.unwrap_or_else(|| Cow::Owned("N/A".to_string())).to_string();
        let method = route.method.to_string();
        let url = format!("{}{}", base_path, route.uri.to_string());
        let rank = route.rank;
        let media_type = match route.format {
            Some(format) => format.to_string(),
            None => "N/A".to_string()
        };
        values.push(vec![
            format!("{:^width$}", name, width= name.len() + 2),
            format!("{:^width$}", method, width= method.len() + 2),
            format!("{:^width$}", url, width= url.len() + 2),
            format!("{:^width$}", rank, width= format!("{}", rank).len() + 2),
            format!("{:^width$}", media_type, width= media_type.len() + 2)
        ]);
    }
    for row in values.clone() {
        for index in 0..column_names.len() {
            let mut column_name = column_names.get(index).unwrap().clone();
            let value = row.get(index).unwrap().clone();
            if column_name.len() < value.len() {
                column_name = format!("{:^width$}", column_name, width=value.len());
            }
            column_names[index] = column_name;
        }
    }
    let mut rows_to_show: Vec<String> = Vec::new();
    for row in values.clone() {
        let mut row_to_show: Vec<String> = Vec::new();
        for index in 0..column_names.len() {
            let column_name = column_names.get(index).unwrap().clone();
            let mut value = row.get(index).unwrap().clone();
            if column_name.len() > value.len() {
                value = format!("{:^width$}", value, width=column_name.len());
            }
            row_to_show.push(value);
        }
        rows_to_show.push(row_to_show.join("|"));
    }
    colored_log(level, format!("|{:-^width$}|", format!("| {} |", "Loaded Routes"), width=column_names.join("|").len()), LogColor::BLUE);
    colored_log(level, format!("|{}|", column_names.join("|")), LogColor::BLUE);
    colored_log(level, format!("|{:-^width$}|", "", width=column_names.join("|").len()), LogColor::BLUE);
    for row in rows_to_show.clone() {
        colored_log(level, format!("|{}|", row), LogColor::BLUE);
    }
    colored_log(level, format!("|{:-^width$}|", "", width=column_names.join("|").len()), LogColor::BLUE);
    return rocket;
}