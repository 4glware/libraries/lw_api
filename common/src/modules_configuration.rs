use serde::{Deserialize, Serialize};
use crate::stop;

#[derive(Debug, Clone, PartialEq, Deserialize, Serialize)]
pub struct StorageConfiguration {
    pub enabled: bool,
    pub database: String
}

#[derive(Debug, Clone, PartialEq, Deserialize, Serialize)]
pub struct EmailConfiguration {
    pub enabled: bool,
    pub database: String
}

#[derive(Debug, Clone, PartialEq, Deserialize, Serialize)]
pub struct SecurityConfiguration {
    pub enabled: bool,
    pub database: String,
    pub jwt_secret: String,
    pub identity_header_name: Option<String>,
    pub time_to_live: i64
}

#[derive(Debug, Clone, PartialEq, Deserialize, Serialize)]
pub struct BillingConfiguration {
    pub enabled: bool,
    pub database: String
}

#[derive(Debug, Clone, PartialEq, Deserialize, Serialize)]
pub struct LWApiModules {
    pub storage: Option<StorageConfiguration>,
    pub billing: Option<BillingConfiguration>,
    pub email: Option<EmailConfiguration>,
    pub security: Option<SecurityConfiguration>,
}

impl LWApiModules {
    pub fn get_module_database(self, module_name: String) -> Option<String> {
        match module_name.as_str() {
            "storage" => self.storage.map(|value| value.database),
            "billing" => self.billing.map(|value| value.database),
            "email" => self.email.map(|value| value.database),
            "security" => self.security.map(|value| value.database),
            wrong_value => {
                stop!("Wrong module name {}", wrong_value);
            }
        }
    }
}