use std::error::Error;
use std::fmt::{Debug, Display, Formatter};
use rocket::http::Status;
use rocket::Request;
use rocket::response::Responder;
use serde::{Serialize, Deserialize};
use crate::ParseableError;

#[derive(Debug)]
pub enum ParsingError {
    /// An I/O error occurred while reading the incoming request data.
    Io(std::io::Error),

    /// The client's data was received successfully but failed to parse as valid
    /// JSON or as the requested type. The `&str` value in `.0` is the raw data
    /// received from the user, while the `Error` in `.1` is the deserialization
    /// error from `serde`.
    Parse(String, serde_json::error::Error),
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ApiError {
    pub status: u16,
    pub error: String
}

impl <'r> Responder<'r, 'static> for ApiError {
    fn respond_to(self, _request: &'r Request<'_>) -> rocket::response::Result<'static> {
        let api_error_json = serde_json::to_string(&self).unwrap();
        rocket::Response::build()
            .sized_body(api_error_json.len(), std::io::Cursor::new(api_error_json))
            .header(rocket::http::ContentType::JSON)
            .status(Status::from_code(self.status).unwrap())
            .ok()
    }
}

impl ParseableError for ApiError {
    fn from(status: Status, error: impl Error) -> Self {
        ApiError {
            status: status.code,
            error: error.to_string()
        }
    }

    fn from_message(status: Status, message: &str) -> Self {
        ApiError {
            status: status.code,
            error: message.to_string()
        }
    }
}

impl ApiError {
    pub fn from_message(status: Status, error: String) -> ApiError {
        ApiError {
            status: status.code,
            error
        }
    }
    pub fn from_result<R, E: Error>(status: Status, result: Result<R, E>) -> ApiError {
        let error = result.err().unwrap();
        ApiError {
            status: status.code,
            error: error.to_string()
        }
    }
}

impl Display for ApiError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.error.as_str())
    }
}

impl Error for ApiError {}