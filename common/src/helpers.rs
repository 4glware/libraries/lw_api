use std::error::Error;
use std::str::FromStr;
use rocket::http::Status;
use uuid::Uuid;
use crate::{get_configuration, ParseableError};

pub fn map_result<T, E: Error, ME: ParseableError>(status: Status, result: Result<T, E>) -> Result<T, ME> {
    if result.is_err() {
        let error = result.err().unwrap();
        return Err(ME::from(status, error));
    }
    return Ok(result.unwrap());
}

pub fn map_option<T, E: ParseableError>(option: Option<T>, status: Status, error_message: &str) -> Result<T, E> {
    if option.is_some() {
        return Ok(option.unwrap());
    }
    return Err(E::from_message(status, error_message));
}

pub fn generate_uuid() -> Uuid {
    let identifier_seed = get_configuration().identifier_seed;
    Uuid::new_v5(&Uuid::from_str(identifier_seed.as_str()).unwrap(), Uuid::new_v4().as_bytes())
}
