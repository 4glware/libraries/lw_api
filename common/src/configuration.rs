use dotenv::{dotenv, var};
use figment::{Figment, providers::{Env, Format, Json, Toml, Yaml}};
use figment::value::Dict;
use rocket::{Build, Rocket};
use serde::{Deserialize, Serialize};
use serde_json::{from_str, to_string};
use crate::{LWApiModules, stop};

static CONFIGURATION_NAME: &str = "lw-api";
static mut LOADED_CONFIGURATION: String = String::new();

#[derive(Debug, Clone, PartialEq, Deserialize, Serialize)]
pub struct DatabaseConfiguration {
    pub name: String,
    pub url: String,
}

#[derive(Debug, Clone, PartialEq, Deserialize, Serialize)]
pub struct Configuration {
    pub ident: String,
    pub address: String,
    pub port: i16,
    pub workers: i16,
    pub keep_alive: i16,
    pub temp_dir: String,
    pub log_level: String,
    pub cli_colors: bool,
    pub secret_key: Option<String>,
    pub modules: Option<LWApiModules>,
    pub log_file: Option<String>,
    pub databases: Option<Dict>,
    pub use_default_catchers: bool,
    pub identifier_seed: String
}

pub fn setup_configuration(mut rocket: Rocket<Build>) -> Rocket<Build> {
    dotenv().ok();
    let profile_env = var("PROFILE");
    let figment = Figment::new()
        .merge(rocket.figment())
        .merge(Toml::file(format!("{}.toml", CONFIGURATION_NAME).as_str()).nested())
        .merge(Yaml::file(format!("{}.yml", CONFIGURATION_NAME).as_str()).nested())
        .merge(Yaml::file(format!("{}.yaml", CONFIGURATION_NAME).as_str()).nested())
        .merge(Env::prefixed(format!("{}_", CONFIGURATION_NAME.to_uppercase()).as_str()))
        .merge(Json::file(format!("{}.json", CONFIGURATION_NAME).as_str()).nested());
    rocket = rocket.configure(figment.clone());
    let configuration = match profile_env {
        Ok(profile) => figment.clone().select(profile).extract::<Configuration>(),
        Err(_error) => figment.clone().extract::<Configuration>()
    };
    if configuration.is_err() {
        let error = configuration.err().unwrap();
        panic!("Fail loading server configuration: {:?}", error.to_string());
    }
    let configuration = configuration.unwrap();
    let configuration_json = to_string(&configuration).unwrap();
    unsafe {
        LOADED_CONFIGURATION = configuration_json;
    }
    return rocket;
}

pub fn get_configuration() -> Configuration {
    return unsafe {
      from_str::<Configuration>(LOADED_CONFIGURATION.as_str()).unwrap()
    };
}
pub fn get_database_configuration(module_name: String) -> DatabaseConfiguration {
    let configuration = get_configuration();
    let modules = configuration.modules;
    if modules.is_none() {
        stop!("Error trying to parse database configuration, no modules found");
    }
    let modules = modules.unwrap();

    let module_database = modules.get_module_database(module_name.clone());

    if module_database.is_none() {
        stop!("Error loading database module, module {} not found", module_name);
    }
    return DatabaseConfiguration {
        name: module_name.clone(),
        url: module_database.unwrap(),
    };
}