#[macro_export]
macro_rules! with_id {
    (
        $struct_name: ident {
            $($field_name: ident : $field_value: expr,)*
        }
    ) => {
        {
            $struct_name {
                _id: Some(lw_api_common::helpers::generate_uuid().to_string()),
                $($field_name: $field_value,)*
            }
        }
    }
}