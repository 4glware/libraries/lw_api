#![feature(map_first_last)]
#![feature(async_closure)]

extern crate core;

pub mod providers;
pub mod models;
pub mod repository;
pub mod search;

use rocket::{Build, Rocket};
use lw_api_common::{ModuleDatabase, stop};
pub use providers::*;
use crate::mongo::MongoDBConnection;
pub use repository::*;
pub use search::*;

pub struct Database<DB: ModuleDatabase + Clone> {
    pub db: DB
}

#[rocket::async_trait]
impl <'r, DB: ModuleDatabase + Clone> rocket::request::FromRequest<'r> for Database<DB> {
    type Error = ();

    async fn from_request(_request: &'r rocket::request::Request<'_>) -> rocket::request::Outcome<Self, Self::Error> {
        rocket::request::Outcome::Success(Database { db: DB::new() })
    }
}

impl <DB: ModuleDatabase + Clone> Clone for Database<DB> {
    fn clone(&self) -> Self {
        Database {
            db: self.db.clone()
        }
    }
}

impl <DB: ModuleDatabase + Clone> Database<DB> {
    pub fn setup(rocket: Rocket<Build>) -> Rocket<Build> {
        let module_name: String = DB::get_module_name();
        if rocket.figment().find_value(format!("modules.{}.database", module_name).as_str()).is_err() {
            stop!("No database url configuration found under database.{}.url", module_name);
        }
        return rocket;
    }
    pub async fn connect(self) -> MongoDBConnection {
        let module_name: String = DB::get_module_name();
        MongoDBConnection::new(module_name).await
    }
    pub async fn new_connection() -> MongoDBConnection {
        let module_name: String = DB::get_module_name();
        MongoDBConnection::new(module_name).await
    }
}