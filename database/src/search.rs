use std::fmt::Debug;
use mongodb::bson::Document;
use serde::{Serialize, Deserialize};
use lw_api_codegen::{RequestBody, ResponseBody};
use lw_api_common::{SearchFilter, SearchPagination};

pub type CustomQuery = &'static (dyn Fn() -> Document + Sync);

#[derive(Debug, Clone, Serialize, Deserialize, RequestBody)]
pub struct SearchParams  {
    pub query: Option<Document>,
    pub custom_query_name: Option<String>,
    pub search_filter: Option<SearchFilter>,
    pub sort: Option<Document>,
    pub pagination: Option<SearchPagination>,
    pub filter_fields: Option<Vec<String>>
}

#[derive(Debug, Clone, Serialize, Deserialize, ResponseBody)]
pub struct SearchResult<T> where T: Debug {
    pub current_page: i64,
    pub total_pages: i64,
    pub page_size: i64,
    pub result: Vec<T>
}

