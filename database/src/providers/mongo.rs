use std::future::Future;
use mongodb::{Client, Database};
use mongodb::options::ClientOptions;
use tokio::task::{JoinHandle, spawn_blocking};
use lw_api_common::{error, get_configuration, get_database_configuration, stop};

pub struct MongoDBConnection {
    pub db: Database
}

impl MongoDBConnection {
    pub async fn new(module_name: String) -> MongoDBConnection {
        let configuration = get_configuration();
        let database_configuration = get_database_configuration(module_name);
        let client = build_client(database_configuration.url, configuration.ident, database_configuration.name.clone()).await;
        if client.is_err() {
            let error = client.err().unwrap();
            stop!("Error trying to get db client: {:?}", error);
        }
        return MongoDBConnection {
            db: client.unwrap().database(database_configuration.name.clone().as_str())
        };
    }
    pub fn run<F, R>(self, function: F) -> JoinHandle<R>
        where F: FnOnce(Database) -> R + Send + 'static,
              R: Send + 'static
    {
        let database = self.db.clone();
        spawn_blocking(move || {
            function(database)
        })
    }
    pub async fn run_async<F, R, Fut>(self, function: F) -> R
        where F: FnOnce(Database) -> Fut + Send + 'static,
              R: Send + 'static,
              Fut: Future<Output=R> + Send + 'static,
    {
        let database = self.db.clone();
        function(database).await
    }
    pub async fn run_async_with_params<F, R, Fut, Params>(self, function: F, params: Params) -> R
        where F: FnOnce(Database, Params) -> Fut + Send + 'static,
              R: Send + 'static,
              Fut: Future<Output=R> + Send + 'static,
    {
        let database = self.db.clone();
        function(database, params).await
    }
}

pub async fn build_client(connection_url: String, app_name: String, database_name: String) -> mongodb::error::Result<Client> {
    let connection_options = ClientOptions::parse(connection_url.as_str()).await;
    if connection_options.is_err() {
        let error = connection_options.err().unwrap();
        error!("Error trying to connect to database {:?}", error);
        return Result::Err(error);
    }
    let mut connection_options = connection_options.unwrap();
    connection_options.app_name = Some(app_name);
    connection_options.default_database = Some(database_name);
    Client::with_options(connection_options)
}
