use serde::{Serialize, Deserialize};
use lw_api_common::{ApiError, Entity, get_configuration, SecurityConfiguration};
use jwt::{SignWithKey, VerifyWithKey, Error as JWTError};
use hmac::{Hmac, Mac};
use mongodb::bson::DateTime;
use rocket::http::Status;
use rocket::Request;
use rocket::request::{FromRequest, Outcome};
use sha2::Sha384;
use lw_api_codegen::{RequestBody, Entity};
use crate::{MongoDBConnection, Repository};

const GENESIS_NONCE: usize = 0;
const IDENTITY_HEADER_NAME: &str = "user-identity";

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct UserToken {
    nonce: usize,
    user_id: String,
    signature: String,
    ttl: i64
}

impl UserToken {
    pub fn generate_token(user_id: String) -> String {
        let configuration: SecurityConfiguration = get_configuration().modules.unwrap().security.unwrap();
        let key: Hmac<Sha384> = Hmac::new_from_slice(&configuration.jwt_secret.as_bytes()).unwrap();
        let signature = GENESIS_NONCE.sign_with_key(&key).unwrap();
        let user_token: UserToken = UserToken {
            user_id,
            nonce: GENESIS_NONCE,
            ttl: DateTime::now().timestamp_millis() + configuration.time_to_live,
            signature
        };
        user_token.sign_with_key(&key).unwrap()
    }
    pub fn validate_token(token: String) -> Result<UserToken, ApiError> {
        let configuration: SecurityConfiguration = get_configuration().modules.unwrap().security.unwrap();
        let key: Hmac<Sha384> = Hmac::new_from_slice(&configuration.jwt_secret.as_bytes()).unwrap();
        let decrypted_token: Result<UserToken, JWTError> = token.as_str().verify_with_key(&key);
        if decrypted_token.is_ok() {
            let user_token: UserToken = decrypted_token.unwrap();
            let current_time_stamp = DateTime::now().timestamp_millis();
            if user_token.ttl < current_time_stamp {
                return Err(ApiError::from_message(Status::Forbidden, "Invalid Token".to_string()));
            }
            return Ok(user_token);
        }
        return Err(ApiError::from_message(Status::Forbidden, "Invalid Token".to_string()));
    }
    pub async fn validate_user(token: String) -> Result<User, ApiError> {
        let user_token = UserToken::validate_token(token)?;
        let connection: MongoDBConnection = MongoDBConnection::new("security".to_string()).await;
        let repository: Repository<User> = Repository::new(connection.db);
        let user = repository.find_by_id(user_token.user_id).await;
        if user.is_err() {
            return Err(ApiError::from_message(Status::Forbidden, "Invalid Token".to_string()));
        }
        let user = user.unwrap();
        return Ok(user);
    }
}


#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct AuthorizedUser {
    pub _id: Option<String>,
    pub first_name: String,
    pub last_name: String,
    pub email: String,
    pub image: String,
    pub username: String,
    pub two_factor_auth_enabled: bool,
    pub phone_number: Option<String>,
    pub current_token: Option<String>
}

#[rocket::async_trait]
impl <'r> FromRequest<'r> for AuthorizedUser {
    type Error = ApiError;

    async fn from_request(request: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        let configuration: SecurityConfiguration = get_configuration().modules.unwrap().security.unwrap();
        if configuration.enabled {
            let header_name = configuration.identity_header_name.unwrap_or(IDENTITY_HEADER_NAME.to_string());
            let identity_header = request.headers().contains(header_name.as_str());
            if identity_header {
                let identity_header = request.headers().get_one(header_name.as_str());
                if identity_header.is_some() {
                    let token = identity_header.unwrap();
                    let user = UserToken::validate_user(token.to_string()).await;
                    if user.is_ok() {
                        return Outcome::Success(AuthorizedUser::from(user.unwrap()));
                    }
                    return Outcome::Failure((Status::Forbidden, user.err().unwrap()));
                }
                return Outcome::Failure((Status::Forbidden, ApiError::from_message(Status::Forbidden, "Invalid Token".to_string())));
            }
            return Outcome::Failure((Status::Forbidden, ApiError::from_message(Status::Forbidden, "Invalid Token".to_string())));
        }
        return Outcome::Failure((Status::InternalServerError, ApiError::from_message(Status::InternalServerError, "Security module disabled, you can't use the user entity.".to_string())));
    }
}

impl From<User> for AuthorizedUser {
    fn from(user: User) -> Self {
        AuthorizedUser {
            _id: user._id,
            image: user.image,
            email: user.email,
            first_name: user.first_name,
            last_name: user.last_name,
            current_token: user.current_token,
            phone_number: user.phone_number,
            two_factor_auth_enabled: user.two_factor_auth_enabled,
            username: user.username
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, Entity)]
pub struct User {
    pub _id: Option<String>,
    pub first_name: String,
    pub last_name: String,
    pub email: String,
    pub image: String,
    pub username: String,
    pub password: String,
    pub two_factor_auth_enabled: bool,
    pub phone_number: Option<String>,
    pub current_token: Option<String>
}

#[derive(Debug, Clone, Serialize, Deserialize, RequestBody)]
pub struct SignInRequest {
    pub credential: String,
    pub password: String
}

#[derive(Debug, Clone, Serialize, Deserialize, RequestBody)]
pub struct SignUpRequest {
    pub first_name: String,
    pub last_name: String,
    pub email: String,
    pub username: String,
    pub image: String,
    pub phone_number: Option<String>,
    pub password: String
}

#[derive(Debug, Clone, Serialize, Deserialize, RequestBody)]
pub struct ChangePasswordRequest {
    pub current_password: String,
    pub password: String
}

#[derive(Debug, Clone, Serialize, Deserialize, RequestBody)]
pub struct ResetPasswordRequest {
    pub reset_code: String
}

#[derive(Debug, Clone, Serialize, Deserialize, RequestBody)]
pub struct TwoFactorRequest {
    pub code_hash: String
}

#[derive(Serialize, Deserialize)]
pub struct AuthToken {
    pub identifier: String,
    pub signature: String,
    pub last_check: String,
    pub current_check: String,
    pub ttl: u32
}