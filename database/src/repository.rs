use std::collections::HashMap;
use std::fmt::Debug;
use mongodb::{Collection, Database as MongoDB};
use mongodb::bson::{doc, Document};
use mongodb::options::{FindOptions};
use rocket::futures::{TryStreamExt};
use serde::de::DeserializeOwned;
use serde::Serialize;
use lw_api_common::{Entity, map_option, map_result, ApiError, CalculatedPagination, SearchPagination, colored_debug};
use crate::{CustomQuery, SearchParams, SearchResult};
use num_integer::Integer;
use rocket::http::Status;
use lw_api_common::logging::LogColor;

pub struct Repository<T: Clone + Entity + Serialize + DeserializeOwned + Unpin + Send + Sync> {
    collection: Collection<T>,
    custom_queries: HashMap<&'static str, CustomQuery>
}

impl <T: Clone + Entity + Serialize + DeserializeOwned + Unpin + Send + Sync + Debug> Repository<T> {
    pub fn new(database: MongoDB) -> Self {
        Repository {
            collection: database.collection(T::get_collection().as_str()),
            custom_queries: HashMap::new()
        }
    }
    pub fn add_custom_query(&mut self, name: &'static str, query: CustomQuery) {
        self.custom_queries.insert(name, query);
    }
    pub async fn bulk_save(&self, entities: Vec<T>) -> Result<Vec<String>, ApiError> {
        let insert_result = map_result(Status::InternalServerError, self.collection.insert_many(entities, None).await)?.inserted_ids;
        let mut result: Vec<String> = Vec::new();
        for inserted_id in insert_result.values() {
            result.push(inserted_id.to_string());
        }
        return Ok(result);
    }
    pub async fn save(&self, entity: T) -> Result<T, ApiError> {
        let inserted_id = map_result(Status::InternalServerError, self.collection.insert_one(entity, None).await)?.inserted_id;
        let entity = map_result(Status::InternalServerError, self.collection.find_one(doc! {
            "_id": inserted_id
        }, None).await)?;
        return map_option(entity, Status::NotFound, format!("{} not found", T::get_name()).as_str());
    }
    pub async fn bulk_update(&self, entities: Vec<T>) -> Result<Vec<T>, ApiError> {
        let entities_to_find = entities.iter().map(|entity| entity.clone().get_id().unwrap()).collect::<Vec<String>>();
        let found_entities = self.find_by_ids(entities_to_find).await?;
        if found_entities.len() < entities.len() {
            return Err(ApiError::from_message(Status::PreconditionFailed,"Some entities were not found".to_string()));
        }
        let mut updated_ids: Vec<String> = Vec::new();
        for entity in entities {
            let updated_entity = map_result(Status::InternalServerError, self.collection.update_one(entity.get_query_by_id_document(), entity.get_document(), None).await)?;
            if updated_entity.modified_count <= 0 {
                return Err(ApiError::from_message(Status::InternalServerError, format!("Fail trying to update entity with id {}", entity.clone().get_id().unwrap())));
            }
            updated_ids.push(updated_entity.upserted_id.unwrap().to_string());
        }
        return map_result(Status::InternalServerError, self.find_by_ids(updated_ids).await);
    }
    pub async fn update(&self, id: String, entity: T) -> Result<T, ApiError> {
        self.find_by_id(id.clone()).await?;
        let result = map_result(Status::InternalServerError, self.collection.update_one(entity.get_query_by_id_document().clone(), entity.get_document().clone(), None).await)?;
        return if result.modified_count > 0 {
            self.find_by_id(id.clone()).await
        } else {
            Err(ApiError::from_message(Status::InternalServerError, format!("Failed to update {}", T::get_name())))
        }
    }
    pub async fn find_all(&self) -> Result<Vec<T>, ApiError> {
        let mut cursor = map_result(Status::InternalServerError, self.collection.find(None, None).await)?;
        let mut results: Vec<T> = Vec::new();
        while let Some(entity) = map_result(Status::InternalServerError, cursor.try_next().await)? {
            results.push(entity);
        }
        return Ok(results);
    }
    pub async fn find_by_id(&self, id: String) -> Result<T, ApiError> {
        let entity = map_result(Status::InternalServerError, self.collection.find_one(doc! {
            "_id": id
        }, None).await)?;
        return map_option(entity, Status::NotFound, format!("{} not found", T::get_name()).as_str());
    }
    pub async fn find_by_ids(&self, ids: Vec<String>) -> Result<Vec<T>, ApiError> {
        let mut cursor = map_result(Status::InternalServerError, self.collection.find(doc! {
            "_id": ids
        }, None).await)?;
        let mut result: Vec<T> = Vec::new();
        while let Some(entity) = map_result(Status::InternalServerError, cursor.try_next().await)? {
            result.push(entity);
        };
        return Ok(result);
    }
    pub async fn bulk_delete(&self, ids: Vec<String>) -> Result<(), ApiError> {
        let entities = self.find_by_ids(ids.clone()).await?;
        let mut deleted_ids: Vec<String> = Vec::new();
        for entity in entities {
            let delete_result = map_result(Status::InternalServerError, self.collection.delete_one(entity.get_query_by_id_document(), None).await)?;
            if delete_result.deleted_count <= 0 {
                return Err(ApiError::from_message(Status::InternalServerError, format!("Error trying to delete entity with id {}", entity.clone().get_id().unwrap())));
            }
            deleted_ids.push(entity.clone().get_id().unwrap());
        }
        if deleted_ids.len() < ids.len() {
            return Err(ApiError::from_message(Status::InternalServerError, "Error trying to delete some entities".to_string()));
        }
        return Ok(());
    }
    pub async fn delete(&self, id: String) -> Result<(), ApiError> {
        let entity = self.find_by_id(id.clone()).await?;
        let delete_result = map_result(Status::InternalServerError, self.collection.delete_one(entity.get_query_by_id_document(), None).await)?;
        if delete_result.deleted_count <= 0 {
            return Err(ApiError::from_message(Status::InternalServerError, "Error trying to delete".to_string()));
        }
        return Ok(());
    }
    pub async fn search<P: Clone + DeserializeOwned + Unpin + Send + Sync + Debug>(&self, search_request: SearchParams) -> Result<SearchResult<P>, ApiError> {
        let current_page = match search_request.pagination.clone() {
            Some(pagination) => {
                match pagination.current_page {
                    Some(current_page_value) => current_page_value,
                    None => 1
                }
            },
            None => 1
        };
        let page_size = match search_request.pagination.clone() {
            Some(pagination) => {
                match pagination.page_size {
                    Some(page_size_value) => page_size_value,
                    None => 10
                }
            },
            None => 10
        };
        let calculated_pagination: CalculatedPagination;
        let mut result: Vec<P> = Vec::new();
        if search_request.query.is_some() {
            let query = search_request.query.unwrap();
            let filter_fields = search_request.filter_fields.clone();
            let sort = search_request.sort.clone();
            colored_debug(format!("Executing {:?}", query.clone()).as_str(), LogColor::YELLOW);
            calculated_pagination = self.calculate_pagination(query.clone(), search_request.pagination).await?;
            let mut cursor = map_result(
                Status::InternalServerError,
                self.collection.clone_with_type::<P>().find(
                    query.clone(),
             FindOptions::builder()
                 .skip(calculated_pagination.start_row as u64)
                 .projection(T::get_projection_document(filter_fields)?)
                 .limit(page_size)
                 .sort(sort)
                 .build()).await)?;
            while let Some(entity) = map_result(Status::InternalServerError, cursor.try_next().await)? {
                result.push(entity);
            }
        } else if search_request.custom_query_name.is_some() {
            let query = self.custom_queries.get(search_request.custom_query_name.clone().unwrap().as_str());
            let filter_fields = search_request.filter_fields.clone();
            let sort = search_request.sort.clone();
            if query.is_some() {
                let custom_query = query.unwrap();
                let query = custom_query();
                colored_debug(format!("Executing {:?}", query.clone()).as_str(), LogColor::YELLOW);
                calculated_pagination = self.calculate_pagination(query.clone(), search_request.pagination).await?;
                let mut cursor = map_result(Status::InternalServerError,
                                            self.collection.clone_with_type::<P>().find(
                        query.clone(),
                        FindOptions::builder()
                            .skip(calculated_pagination.start_row as u64)
                            .projection(T::get_projection_document(filter_fields)?)
                            .limit(page_size)
                            .sort(sort)
                            .build()).await)?;
                while let Some(entity) = map_result(Status::InternalServerError, cursor.try_next().await)? {
                    result.push(entity);
                }
            } else {
                return Err(ApiError::from_message(Status::NotFound,format!("custom_query_name {} not found", search_request.custom_query_name.unwrap())));
            }
        } else if search_request.search_filter.is_some() {
            let search_filter = search_request.search_filter.unwrap();
            let filter_fields = search_request.filter_fields.clone();
            let sort = search_request.sort.clone();
            colored_debug(format!("Executing {:?}", T::get_search_filter_document(search_filter.clone())?).as_str(), LogColor::YELLOW);
            calculated_pagination = self.calculate_pagination(T::get_search_filter_document(search_filter.clone())?, search_request.pagination).await?;
            let mut cursor = map_result(
                Status::InternalServerError,
                self.collection.clone_with_type::<P>().find(
                    T::get_search_filter_document(search_filter.clone())?,
                    FindOptions::builder()
                        .skip(calculated_pagination.start_row as u64)
                        .projection(T::get_projection_document(filter_fields.clone())?)
                        .limit(page_size)
                        .sort(sort)
                        .build()).await)?;
            while let Some(entity) = map_result(Status::InternalServerError, cursor.try_next().await)? {
                result.push(entity);
            }
        } else {
            return Err(ApiError::from_message(Status::PreconditionFailed, "You have to specify one of these fields: query, custom_query_name or search_filter in order to search.".to_string()));
        }
        return Ok(SearchResult {
            current_page,
            page_size,
            total_pages: calculated_pagination.total_pages,
            result
        });
    }
    async fn calculate_pagination(&self, filter: Document, search_pagination: Option<SearchPagination>) -> Result<CalculatedPagination, ApiError> {
        let current_page = match search_pagination.clone() {
            Some(pagination) => {
                match pagination.current_page {
                    Some(current_page_value) => current_page_value,
                    None => 1
                }
            },
            None => 1
        };
        let page_size = match search_pagination.clone() {
            Some(pagination) => {
                match pagination.page_size {
                    Some(page_size_value) => page_size_value,
                    None => 10
                }
            },
            None => 10
        };
        if page_size <= 0 {
            return Err(ApiError::from_message(Status::BadRequest,"Invalid page_size value, has to be greater than 0".to_string()));
        }
        let total_rows = map_result(Status::InternalServerError, self.collection.count_documents(filter, None).await)? as i64;
        let total_pages = total_rows.div_ceil(&page_size);
        if total_rows > 0 && current_page <= 0 ||
            total_rows > 0 && current_page > total_pages {
            return Err(ApiError::from_message(Status::BadRequest,format!("Invalid current_page value, has to be between {} and {}", 1, total_pages)));
        }
        return Ok(CalculatedPagination {
            total_pages,
            start_row: (current_page - 1) * page_size
        });
    }
}