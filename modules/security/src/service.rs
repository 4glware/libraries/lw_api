use lw_api_database::models::security::{ChangePasswordRequest, ResetPasswordRequest, SignInRequest, SignUpRequest, TwoFactorRequest, User};
use lw_api_database::{Repository};
use lw_api_codegen::Service;
use lw_api_common::{with_id};
use crate::database::SecurityDatabase;

#[derive(Service)]
#[service(db_type=SecurityDatabase)]
pub struct SecurityService {
    user_repository: Repository<User>,
}

impl SecurityService {
    pub async fn sign_in(self, request: SignInRequest) {
        // generate the token after validating credentials

    }
    pub async fn sign_out(self) {
        // Clean current token
    }
    pub async fn sign_up(self, request: SignUpRequest) {
        // register user

        let password_hash = request.password;

        self.user_repository.save(with_id!(User {
            email: request.email,
            first_name: request.first_name,
            last_name: request.last_name,
            current_token: None,
            image: request.image,
            password: password_hash,
            phone_number: request.phone_number,
            two_factor_auth_enabled: false,
            username: request.username,
        })).await.unwrap();
    }
    pub async fn change_password(self, request: ChangePasswordRequest) {
        // verify current password and change it with the new one
    }
    pub async fn two_factor(self, request: TwoFactorRequest) {
        // two_factor auth, we verify the hash generated from the code entered
    }
    pub async fn reset_password(self, request: ResetPasswordRequest) {
        // generate a url to reset the password and email the user with that generated temporal url
    }
}