use rocket::{post, put};
use lw_api_database::models::security::{ChangePasswordRequest, ResetPasswordRequest, SignInRequest, SignUpRequest, TwoFactorRequest, AuthorizedUser};
use crate::service::SecurityService;

#[lw_api_codegen::controller("/security")]
pub fn security_controller() {
    #[post("/signIn", format = "application/json", data = "<request>")]
    async fn sign_in(service: SecurityService, request: SignInRequest) {
        service.sign_in(request).await;
    }

    #[post("/signOut")]
    async fn sign_out(service: SecurityService, auth_user: AuthorizedUser) {
        // TODO: finish the security service in order to have the login system working
        service.sign_out().await;
    }
    #[post("/signUp", format = "application/json", data = "<request>")]
    async fn sign_up(service: SecurityService, request: SignUpRequest) {
        service.sign_up(request).await;
    }
    #[put("/change", format = "application/json", data = "<request>")]
    async fn change_password(service: SecurityService, request: ChangePasswordRequest) {
        service.change_password(request).await;
    }
    #[put("/twoFactor", format = "application/json", data = "<request>")]
    async fn two_factor(service: SecurityService, request: TwoFactorRequest) {
        service.two_factor(request).await;
    }
    #[post("/reset", format = "application/json", data = "<request>")]
    async fn reset_password(service: SecurityService, request: ResetPasswordRequest) {
        service.reset_password(request).await;
    }
}