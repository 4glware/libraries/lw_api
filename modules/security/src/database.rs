use rocket::{Build, Rocket};
use lw_api_codegen::ModuleDatabase;
use lw_api_database::Database;

#[derive(Clone, ModuleDatabase)]
#[database_name("security")]
pub struct SecurityDB;

pub type SecurityDatabase = Database<SecurityDB>;

pub fn setup(rocket: Rocket<Build>) -> Rocket<Build> {
    SecurityDatabase::setup(rocket)
}