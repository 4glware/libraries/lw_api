use rocket::{Build, Rocket};
use lw_api_codegen::ModuleDatabase;
use lw_api_database::Database;

#[derive(Clone, ModuleDatabase)]
#[database_name("storage")]
pub struct StorageDB;

pub type StorageDatabase = Database<StorageDB>;

pub fn setup(rocket: Rocket<Build>) -> Rocket<Build> {
    StorageDatabase::setup(rocket)
}