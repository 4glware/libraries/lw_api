#![feature(async_closure)]

use rocket::{Build, Rocket};
use lw_api_common::debug;

pub mod controller;
pub mod database;

pub fn setup(mut rocket: Rocket<Build>) -> Rocket<Build> {
    debug!("Loading storage module...");
    rocket = controller::storage_controller()(rocket);
    rocket = database::setup(rocket);
    return rocket;
}