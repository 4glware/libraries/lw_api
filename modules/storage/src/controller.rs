use std::time::Duration;
use mongodb::{Database};
use rocket::{get, post};
use rocket::tokio::time::sleep;
use lw_api_codegen::{RequestBody, ResponseBody};
use serde::{Serialize, Deserialize};
use crate::database::StorageDatabase;

#[derive(Debug, Clone, Serialize, Deserialize, RequestBody, ResponseBody)]
pub struct Hello {
    name: String,
}

#[lw_api_codegen::controller("/storage")]
pub fn storage_controller() {
    #[get("/hello")]
    async fn hello() -> &'static str {
        sleep(Duration::from_secs(10)).await;
        "Hello World"
    }
    #[post("/echo", format = "application/json", data = "<hello>")]
    async fn echo(hello: Hello) -> Hello {
        hello
    }
    #[post("/hello", format = "application/json", data = "<hello>")]
    async fn fetch(database: StorageDatabase, hello: Hello) -> String {
        database.connect().await.run_async_with_params(test_db, hello).await
    }
}

pub async fn test_db(database: Database, hello: Hello) -> String {
    let collections = database.list_collection_names(None).await.unwrap();
    println!("Collections {:?}", collections);
    return format!("Hello {:?}", hello.name);
}