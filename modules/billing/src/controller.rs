use rocket::{get};

#[lw_api_codegen::controller("/billing")]
pub fn billing_controller() {
    #[get("/hello")]
    async fn hello() -> &'static str {
        "Hello World"
    }
}