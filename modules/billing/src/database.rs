use rocket::{Build, Rocket};
use lw_api_codegen::ModuleDatabase;
use lw_api_database::Database;

#[derive(Clone, ModuleDatabase)]
#[database_name("billing")]
pub struct BillingDB;

pub type BillingDatabase = Database<BillingDB>;

pub fn setup(rocket: Rocket<Build>) -> Rocket<Build> {
    BillingDatabase::setup(rocket)
}