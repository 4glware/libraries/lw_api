use rocket::{get};

#[lw_api_codegen::controller("/email")]
pub fn email_controller() {
    #[get("/hello")]
    async fn hello() -> &'static str {
        "Hello World"
    }
}