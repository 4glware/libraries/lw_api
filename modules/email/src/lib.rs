use rocket::{Build, Rocket};
use lw_api_common::debug;

pub mod controller;
pub mod database;

pub fn setup(mut rocket: Rocket<Build>) -> Rocket<Build> {
    debug!("Loading email module...");
    rocket = controller::email_controller()(rocket);
    rocket = database::setup(rocket);
    rocket
}