use rocket::{Build, Rocket};
use lw_api_codegen::ModuleDatabase;
use lw_api_database::Database;

#[derive(Clone, ModuleDatabase)]
#[database_name("email")]
pub struct EmailDB;

pub type EmailDatabase = Database<EmailDB>;

pub fn setup(rocket: Rocket<Build>) -> Rocket<Build> {
    EmailDatabase::setup(rocket)
}